<?php

/* YOUR FUNCTIONS GO HERE */

function getAuthors($dbh){
  $query = 'SELECT * FROM author';
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $authors= $stmt->fetchAll(PDO::FETCH_ASSOC);
 return $authors;
}

function getPub($dbh){
  $query = 'SELECT * FROM publisher';
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $publishers= $stmt->fetchAll(PDO::FETCH_ASSOC);
 return $publishers;
}

function getOnePub($dbh, $publisher_id){
  $query = "SELECT * FROM publisher where publisher_id= :publisher_id";
  $stmt = $dbh->prepare($query);
   $params = [':publisher_id'=> $publisher_id];
  $stmt->execute($params);
  $row= $stmt->fetch(PDO::FETCH_ASSOC);
 return $row;
}

function updatePub($dbh){
  $query = "UPDATE publisher
            SEt
            name=:name,
            phone=:phone,
            city=:city
            WHERE
            publisher_id=:publisher_id
            ";
  $stmt = $dbh->prepare($query);
   $params = [':publisher_id'=> $_POST['publisher_id'],
               ':name'=> $_POST['name'],
               ':city'=> $_POST['city'],
               ':phone'=> $_POST['phone']
             
             
             ];
  if($stmt->execute($params)){return true;
                             }
  $row= $stmt->fetch(PDO::FETCH_ASSOC);
 return false;
}

function getFormats($dbh){
  $query = 'SELECT * FROM format';
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $formats= $stmt->fetchAll(PDO::FETCH_ASSOC);
 return $formats;
}


function getGenres($dbh){
  $query = 'SELECT * FROM genre';
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $genres= $stmt->fetchAll(PDO::FETCH_ASSOC);
 return $genres;
}

function getBooks($dbh){
  $query = 'SELECT 
  book.title, 
  author.name as author,
  book.year_published as year,
  book.num_pages as num,
  book.price as price,
  publisher.name as publisher,
  format.name as format,
  genre.name as genre
  FROM book 
  JOIN format USINg (format_id) 
  JOIN genre USING (genre_id) 
  JOIN publisher USING(publisher_id) 
  JOIN author using(author_id)';
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $books= $stmt->fetchAll(PDO::FETCH_ASSOC);
 return $books;
}