<?php

/**
 * Form 3
 * @file 12_form_3.php
 * @course Intro PHP, WDD 2018 Jan
 * @author Navdeep Dhindsa <dhindsanavdeep24@gmail.com>
 * @created_at 2018-08-01
 */
include '0_config.php';
include '10_functions.php';
$title ='Form 3';

//if request method is post
if($_SERVER['REQUEST_METHOD'] == 'POST'){
//var dump post
//endif

//var_dump($_POST);
  //set empty error array
  $errors = [];
  //check first name is required
  required('first_name',$errors);
  //check first name is required
  required('last_name',$errors);
  //make sure email is correct
  validateEmail('email',$errors);
  //var dump errors
  //var_dump($errors);
  
}

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?=$title?></title>
</head>
<body>


<h1><?=$title?></h1>

<h2>Output</h2>

  
  
<form action="12_form_3.php" method="post" accept-charset="UTF-8" novalidate="novalidate">

    <fieldset>

        <legend>Simple Form</legend>

            <p><label for="first_name">First Name</label>
                <input type="text" name="first_name" maxlength="255" value = "<?php
                                                                              
                                                                              
                                                                              if(!empty($_POST['first_name'])){
                                                                                echo esc_attr($_POST['first_name']);
                                                                              }?>"/>
              </p> <?php if(!empty($errors['first_name'])) : ?>
              <span class="errors"><?=esc_attr($errors['first_name'])?></span>
              <?php endif; ?>
              
              
           

            <p><label for="last_name">Last Name</label>
                <input type="text" name="last_name" maxlength="255" value = "<?php
                                                                              
                                                                              
                                                                              if(!empty($_POST['last_name'])){
                                                                                echo esc_attr($_POST['last_name']);
                                                                              }
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              ?>"/>
            </p>
            <?php if(!empty($errors['last_name'])) : ?>
              <span class="errors"><?=esc_attr($errors['last_name'])?></span>
              <?php endif; ?>
            <p><label for="email">Email</label>
                <input type="text" name="email" maxlength="255" value ="<?php
                                                                              
                                                                              
                                                                              if(!empty($_POST['email'])){
                                                                                echo esc_attr($_POST['email']);
                                                                              }
                                                                              
                                                                              
                                                                              
                                                                              
                                                                              ?>"/>
            </p>
       <?php if(!empty($errors['email'])) : ?>
              <span class="errors"><?=esc_attr($errors['email'])?></span>
              <?php endif; ?>

            <p><button type="submit">Submit</button></p>

        </fieldset>

</form>





</body>
</html>