<?php

/**
 * Querying MySQL - Inserting Records
 * @file 17_querying_mysql_inserting_records.php
 * @course Intro PHP, WDD 2018 Jan
 * @author Steve George <edu.pagerange@gmail.com>
 * @created_at 2018-08-03
 */

$title ='Querying MySQL - Inserting Records';

// load the config and functions files
require '../connect_db.inc.php';
require '10_functions.php';

if($_SERVER['REQUEST_METHOD'] == 'POST') {

    $errors = [];

    // validate for required
    $errors = required('first_name', $errors);
    $errors = required('last_name', $errors);
    $errors = required('email', $errors);
    $errors = required('age', $errors);
    $errors = required('password', $errors);
    $errors = required('confirm_password', $errors);

    // validate for email
    $errors = validateEmail('email', $errors);

    // validate for matching passwords
    $errors = passwordsMatch('password', 'confirm_password', $errors);

    // if no errors, insert record
    if(empty($errors)) {

        // Connect to database and set error reporting mode
        $dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // create our insert query
        $query = 'INSERT INTO users 
                (first_name, last_name, email, age, password)
                VALUES
                (:first_name, :last_name, :email, :age, :password)';

        // Send query to mysql, ready for execution
        $stmt = $dbh->prepare($query);

        // bind all insert data to named placeholders for proper mysql escaping
        // note password hash function, using default encryption method
        // This is an alternative to individually binding each value
        $params = array (
            ':first_name' => $_POST['first_name'],
            ':last_name' => $_POST['last_name'],
            ':email' => $_POST['email'],
            ':age' => intval($_POST['age']),
            ':password' => password_hash($_POST['password'], PASSWORD_DEFAULT)
        );

        // Execute the query with our bound parameter values
        // If insert was a success, select the inserted record
        if($stmt->execute($params)) {
            
            // Get the last insert id from the database connection
            $id = $dbh->lastInsertId();

            // Create our new insert query
            $query = 'SELECT * FROM users
                    WHERE id = :id';

            // prepare the query and send to mysql, ready for execution
            $stmt = $dbh->prepare($query);

            // Bind our $id value to the named placeholder
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);

            // Execute the query
            $stmt->execute();

            // fetch just one user... the one we inserted
            $user = $stmt->fetch(PDO::FETCH_ASSOC);

            // Set the success flag to true so we can control
            // HTML flow (show or don't show form)
            $success = true;

        } // end if stmt execute params

    } // end if no errors
    
} // end if POST submission


?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?=esc($title)?></title>
</head>
<body>


<h1><?=esc($title)?></h1>



<form action="" method="post"
    accept-charset="utf-8" novalidate>

    <fieldset>
        <legend>Edit User</legend>

        <p><span class="error">*</span> indicates a required field</p>


        <p class="first_name">
            <label class="required" for="first_name">First Name</label>&nbsp;
            <input type="text" name="first_name" max-length="255"
            value="<?php if(!empty($_POST['first_name'])) 
                    echo esc_attr($_POST['first_name']); ?>" />
            <?php if(!empty($errors['first_name'])) : ?>
            <span class="error"><?=$errors['first_name']?></span>
             <?php endif; ?>
        </p>

        <p class="last_name">
            <label class="required" for="last_name">Last Name</label>&nbsp;
            <input type="text" name="last_name" max-length="255"
            value="<?php if(!empty($_POST['last_name'])) 
                    echo esc_attr($_POST['last_name']); ?>" />
            <?php if(!empty($errors['last_name'])) : ?>
            <span class="error"><?=$errors['last_name']?></span>
             <?php endif; ?>
        </p>

        <p class="email">
            <label class="required" for="email">Email</label>&nbsp;
            <input type="text" name="email" max-length="255"
            value="<?php if(!empty($_POST['email'])) 
                    echo esc_attr($_POST['email']); ?>" />
            <?php if(!empty($errors['email'])) : ?>
            <span class="error"><?=$errors['email']?></span>
             <?php endif; ?>
        </p>

        <p class="age">
            <label class="required" for="age">Age</label>&nbsp;
            <input type="text" name="age" max-length="255"
            value="<?php if(!empty($_POST['age'])) 
                    echo esc_attr($_POST['age']); ?>" />
            <?php if(!empty($errors['age'])) : ?>
            <span class="error"><?=$errors['age']?></span>
             <?php endif; ?>
        </p>

        <p class="password">
            <label class="required" for="password">Password</label>&nbsp;
            <input type="password" name="password" max-length="255" />
            <?php if(!empty($errors['password'])) : ?>
            <span class="error"><?=$errors['password']?></span>
             <?php endif; ?>
        </p>

        <p class="confirm_password">
            <label class="required" for="confirm_password">Confirm Password</label>&nbsp;
            <input type="password" name="confirm_password" max-length="255" />
            <?php if(!empty($errors['confirm_password'])) : ?>
            <span class="error"><?=$errors['confirm_password']?></span>
             <?php endif; ?>
        </p>

        <p>
            <label>&nbsp;</label>&nbsp;
            <button type="submit">Update</button>
        </p>

    </fieldset>

</form>



</body>
</html