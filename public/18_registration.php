<?php
include '10_functions.php';
if($_SERVER['REQUEST_METHOD'] == 'POST'){
  $errors=[];
  var_dump($_POST);
  
  
  
 
  $errors = minLength('first_name',2,$errors);
  
  $errors = required('first_name',$errors);
  
  var_dump($errors);
  
  if(count($errors)==0){
    define('DB_USER','web_user');
    define('DB_PASS','mypass');
    define('DB_DSN','mysql:host=localhost;dbname=testing');
    $dbh= new PDO(DB_DSN, DB_USER, DB_PASS);
    

    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $query = "INSERT INTO users (first_name, last_name) VALUES (:first_name, :last_name)";
    $stmt = $dbh->prepare($query);
    
   // $params =[
//      ':first_name' => $_POST['first_name'],
//      ':last_name' => $_POST['last_name']
  //  ]
    $stmt->bindValue(':first_name', $_POST['first_name'],PDO::PARAM_INT);
    
    $stmt->bindValue(':last_name', $_POST['last_name'],PDO::PARAM_INT);
    $stmt ->execute();
   
  }
  

$query = "SELECT * from users";
$stmt = $dbh->prepare($query);

$stmt ->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
}//end post check
?><!DOCTYPE html>
<html>
  <head>
  <title>Register</title>
  <meta charset="utf-8">
    <style>
      table{
      border:1px solid #000;
      border-collapse: collapse;
    }
    td,th{
      border:1px solid #000;
      border-collapse: collapse;
    }
      fieldset{
        width: 800px;
        margin: 30px auto;
        border: 1px solid #000;
        background: #efefef;
        padding: 30px;
      }
    
    </style>
  </head>
  <body>
  <h1>Register</h1>
  <form action="18_registration.php" method="post" accept-charset="utf-8" novalidate>
    <fieldset>
      <legend>Register</legend>
    <p>
    <label for="first_name" >First Name</label><br />
    <input type="text" name="first_name" maxlength="255" />
    </p>
      <p>
    <label for="last_name" >Last Name</label><br />
    <input type="text" name="last_name" maxlength="255" />
    </p>
      <p>
    <label for="password" >Password</label><br />
    <input type="password" name="password" maxlength="255" />
    </p>
    <p>
    <button type="submit">Register</button>
    </p>
    </fieldset>
    </form>
  
  <table>
  <tr>
  <th>First Name</th>
  <th>Last Name</th>
   
    
    
  </tr>
  <?php foreach($result as $row) : ?>
  <tr>
  <td><?=$row['first_name']?></td>
    <td><?=$row['last_name']?></td>
   
  
  
  </tr>
  <?php endforeach;?>
  
  
  </table>
  </body>
  
  
</html>