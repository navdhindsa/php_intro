<?php

/**
 * Intro PHP - Final Morning
 * @name Your Name
 */

/* DO NOT EDIT THE NEXT 4 LINES */

include 'config.php';
include 'functions.php';

$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

/* DO NOT EDIT ABOVE THIS LINE */

$authors= getAuthors($dbh);
$publishers= getPub($dbh);
$formats= getFormats($dbh);
$genres= getGenres($dbh);
$books= getBooks($dbh);

?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Intro PHP Final Morning</title>
  <style>
    body {
      font-family: Verdana, Arial, sans-serif;
      width: 100%;
      font-size: 80%;
    }
    div {
      box-sizing: border-box;
    }
    .wrapper {
      width: 100%;  
      max-width: 1024px;
      margin: 20px auto;
    }

    .container {
      float: left;
      padding: 15px;
      border: 1px solid #cfcfcf;
    }

    .sidebar {
      width: 25%;
      float: left;
    }

    .main {
      width: 75%;
      float: left;
    }
    .content {
      padding-left: 15px;
    }

    .callout {
      padding: 10px;
      border: 1px solid #cfcfcf;
      margin-bottom: 20px;
    }

    .callout h3 {
      margin-top: 0;
    }

    .books .callout {
      width: 45%;
      float: left;
      margin-top: 10px;
      border: 1px solid #cfcfcf;
      margin-right: 10px;
    }

    ul {
      padding: 0;
    }

    ul li {
      list-style-type: none;
    }

    table {
      border: 1px solid #cfcfcf;
      border-collapse: collapse;
    }

    td, th {
      border: 1px solid #cfcfcf;
      padding: 4px;
    }

    th {
      background: #000;
      color: #fff;
      text-align: left;
    }

  </style>
</head>
<body>

<div class="wrapper">

   <h1>Intro PHP - Final Morning</h1>

  <div class="container">

    <div class="sidebar">

      <h2>Sidebar</h2>

      <div class="callout authors">
        <h3>Authors</h3>

        <ul>
          <?php foreach($authors as $row) : ?>
                      <li><?=$row['name']?></li>
           <?php endforeach; ?>           
                  </ul>

      </div><!-- /.callout -->

      <div class="callout publishers">
        <h3>Publishers</h3>
        <ul>
          <?php foreach($publishers as $row) : ?>
                      <li><?=$row['name']?></li>
                      <?php endforeach; ?>   
                  </ul>


      </div><!-- /.callout -->

    </div><!-- /.sidebar -->

    <div class="main">

      <div class="content books">
        <h2>Books</h2>

        <table>
          <tr>
            <th>Title</th>
            <th>Author</th>
            <th>Year Published</th>
            <th>Num Pages</th>
            <th>Price</th>
            <th>Publisher</th>
            <th>Format</th>
            <th>Genre</th>

          </tr>
 <?php foreach($books as $result) : ?>
              <tr>
            <?php foreach($result as $key => $value) : ?>
              <td><?=$value?></td>
              
              
            <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>
                     
          

        </table>

        <div class="callout formats">
        <h3>Formats</h3>

        <ul>
                      <?php foreach($formats as $row) : ?>
                      <li><?=$row['name']?></li>
           <?php endforeach; ?>  
                      
                  </ul>

      </div><!-- /.callout -->

      <div class="callout genres">
        <h3>Genres</h3>

        <ul>
                       <?php foreach($genres as $row) : ?>
                      <li><?=$row['name']?></li>
           <?php endforeach; ?>  
                      
                  </ul>

      </div><!-- /.callout -->

      </div><!-- /.books -->

    </div><!-- /.main -->

  </div><!-- /.container -->

</div><!-- /.wrapper -->

</body>
</html>

