<?php

/**
 * Intro PHP Practical
 * @name Navdeep Dhindsa
 */

/* DO NOT EDIT THE NEXT 4 LINES */

include 'config.php';
include 'functions.php';

$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

/* DO NOT EDIT ABOVE THIS LINE */

$a1 = first($dbh);
$a2 = second($dbh);
$a3 = third($dbh);
$a4 = fourth($dbh);
$a5 = fifth($dbh);
$a6 = sixth($dbh);
$a7 = seventh($dbh);


?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Intro PHP Practical</title>
  <style>

   .well {
    width: 60%;
    margin: 0;
    padding: 20px;
    background: #efefef;
    border: 1px solid #cfcfcf;
   }
  
    table {
      border-collapse: collapse;
    }
    
    th, td {
      border: 1px solid #cfcfcf;
      padding: 5px 10px;
    }
    
    th {
      text-align: right;
    }
    
    td {
      text-align: center;
      background: #cff7d0;
    }
    
    .container {
      width: 100%;
      max-width: 900px;
      margin: 0px auto 100px;
    }

    .green {
      background: #cff7d0;
      border: 1px solid #cfcfc;
      padding: 2px;
    }
  
  </style>
</head>
<body>

<div class="container">

<h1>Intro PHP Practical</h1>

<div class="well">

<p>Your job is to get this file working, by replacing the static values in the right, <span class="green">green</span> column with dynamic values pulled from your own 'books' database.</p>

<p>Note: You do not need to change this file in ANY WAY, except to replace those static values in the right column.  Leave all styling, and all other HTML alone.</p>

</div>

<h2>Books Database Overview</h2>

<table id="aggregate">
  
    <tr>
        <th>Books in print</th>
      <?php foreach($a1 as $row) : ?>
        <td><?=$row['count(title)']?></td>
      <?php endforeach; ?>
    </tr>
  
    <tr>
      <?php foreach($a2 as $row) : ?>
        <th>Books not in print</th>
        <td><?=$row['count(title)']?></td>
       <?php endforeach; ?>
    </tr>
  
    <tr>
      <?php foreach($a3 as $row) : ?>
        <th>Average Book Price</th>
        <td>$<?=$row['format(avg(price),2)']?></td>
      <?php endforeach; ?>
    </tr>
  
    <tr>
      <?php foreach($a4 as $row) : ?>
        <th>Lowest Book Price</th>
        <td>$<?=$row['min(price)']?></td>
      <?php endforeach; ?>
    </tr>
  
    <tr>
      <?php foreach($a5 as $row) : ?>
        <th>Highest Book Price</th>
        <td>$<?=$row['max(price)']?></td>
      <?php endforeach; ?>
    </tr>
    
     <tr>
       <?php foreach($a6 as $row) : ?>
        <th>Total Publishers</th>
        <td><?=$row['count(name)']?></td>
       <?php endforeach; ?>
    </tr>
  
    <tr>
      <?php foreach($a7 as $row) : ?>
        <th>Total Publishers without Books</th>
        <td><?=$row['count(publisher.name)']?></td>
      <?php endforeach; ?>
    </tr>


  </table><!-- /aggregate -->

</div><!-- /container -->

</body>
</html>

