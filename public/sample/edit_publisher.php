<?php


$title= "Publisher's list";
include 'config.php';
include 'functions.php';

$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$errors=[];


if($_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET['publisher_id'])){
  if(empty($_GET['publisher_id'])){
    header('Location: publisher_list.php');
    die;
  }
  
  $publisher_id = $_GET['publisher_id'];
   $publishers= getOnePub($dbh, $publisher_id);
}
  
  
 


if($_SERVER['REQUEST_METHOD'] == 'POST'){
  if(empty($_POST['publisher_id'])){
    die('publisher id required');
  }
  if(empty($_POST['name'])){
    $errors['name']='name is required';
  }
   if(empty($_POST['city'])){
    $errors['city']='city is required';
  }
   if(empty($_POST['phone'])){
    $errors['phone']='phone is required';
  }
   
  $publishers = $_POST;
  if(empty($errors)){
    updatePub($dbh);
    header('Location: publisher_list.php');
    die;
  }
}

?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Intro PHP Final Morning</title>
  </head>
  <body>
  <h1><?=$title?></h1>
  <form action="edit_publisher.php" method="post" accept-charset="UTF-8" novalidate="novalidate">

    <fieldset>

        <legend>Edit Publisher </legend>

            <p>
                <input type="hidden" name="publisher_id" maxlength="255" value="<?=$publishers['publisher_id'] ?>" />
            </p>

            <p><label for="name">Name</label>
                <input type="text" name="name" maxlength="255" value="<?=$publishers['name'] ?>" />
              <?php if(!empty($errors['name'])) echo $errors['name']; ?>
            </p>
      <p><label for="city">City</label>
                <input type="text" name="city" maxlength="255" value="<?=$publishers['city'] ?>" />
         <?php if(!empty($errors['city'])) echo $errors['city']; ?>
            </p>
      <p><label for="phone">Phone</label>
                <input type="text" name="phone" maxlength="255" value="<?=$publishers['phone'] ?>" />
         <?php if(!empty($errors['phone'])) echo $errors['phone']; ?>
            </p>

            <p><button type="submit">Submit</button></p>

        </fieldset>

    </form>

  
  
  </body>
</html>