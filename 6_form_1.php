<?php

/**
 * Form 1
 * @file 6_form_1.php
 * @course Intro PHP, WDD 2018 Jan
 * @author Steve George <edu.pagerange@gmail.com>
 * @created_at 2018-07-27
 */

$title ='Form 1';

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?=$title?></title>
</head>
<body>
<div id="output">

<h1><?=$title?></h1>

<h2>Output</h2>

<form action="7_handle_form2.php" method="post" accept-charset="UTF-8" novalidate="novalidate">

    <fieldset>

        <legend>Contact Us</legend>

            <p><label for="name">Name</label>
                <input type="text" name="name" maxlength="255" />
            </p>

            <p>
                <label for="gender">Gender</label><br />
                <select name="gender">
                    <option value=''>Select your gender</option>
                    <option value="M">Male</option>
                    <option value="F">Female</option>
                </select>
            </p>

            <p><label for="email">Email</label>
                <input type="text" name="email" maxlength="255" />
            </p>

            <p><label for="comment">Comment</label>
                <textarea name="comment"></textarea></p>

            <p>
                <label for="interests">Interests</label><br />
                <input type="checkbox" name="interests[]" value="c++" />&nbsp;C++<br />
                <input type="checkbox" name="interests[]" value="Python" />&nbsp;Python<br />
                <input type="checkbox" name="interests[]" value="php" />&nbsp;PHP<br />

                <input type="checkbox" name="interests[]" value="java" />&nbsp;Java<br />
                <input type="checkbox" name="interests[]" value="ruby" />&nbsp;Ruby<br />
            </p>
            <hr />
            <p>
                <input type="radio" name="subscribe" value='yes' /> &nbsp;Yes, please subscribe me to your stupid eNewsletter!<br />
                <input type="radio" name="subscribe" value="no" /> &nbsp;No, I don't want your stupid eNewsletter!
            </p>
            <hr />

            <p><button type="submit">Submit</button></p>

            <!--

                comment box (textarea)

                subscription (checkbox)

                radio buttons (interest)

                select dropdown (gender)

            -->

    </fieldset>

</form>

<pre>

    <?php print_r($_SERVER); ?>

</pre>

</div><!-- /#output -->

<div id="source">

    <h2>Source</h2>

    <?php show_source(__FILE__); ?>

</div><!-- /#source -->

</body>
</html>