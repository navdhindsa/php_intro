<?php

/**
 * Form 2
 * @file 8_form_2.php
 * @course Intro PHP, WDD 2018 Jan
 * @author Navdeep <dhindsanavdeep24@gmail.com>
 * @created_at 2018-07-31
 */

include '0_config.php';

$title ='Form 2';

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?=$title?></title>
</head>
<body>
<div id="output">

<h1><?=$title?></h1>

<h2>Output</h2>

<form action="9_handle_form_2.php" method="post" accept-charset="UTF-8" novalidate="novalidate">

    <fieldset>

        <legend>Simple Form</legend>

            <p><label for="first_name">First Name</label>
                <input type="text" name="first_name" maxlength="255" />
            </p>

            <p><label for="last_name">Last Name</label>
                <input type="text" name="last_name" maxlength="255" />
            </p>

            <p><label for="email">Email</label>
                <input type="text" name="email" maxlength="255" />
            </p>

            <p><button type="submit">Submit</button></p>

        </fieldset>

    </form>

</div><!-- /#output -->

<div id="source">

    <h2>Source</h2>

    <?php show_source(__FILE__); ?>

</div><!-- /#source -->

</body>
</html>