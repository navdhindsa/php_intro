<?php
/**
 * Handle Form 2
 * @file 9_handle_form_2.php
 * @course Intro PHP, WDD 2018 Jan
 * @author Steve George <edu.pagerange@gmail.com>
 * @created_at 2018-07-31
 */

// include config and functions file
include '0_config.php';
include '10_functions.php';
 
 // Only allow POST requests
 if($_SERVER['REQUEST_METHOD'] !== 'POST') {
     // commented so you can see the rest of the page
     // die('Please use the form.');
 }


// Set empty errors array
$errors = [];

// Validate fields for required
required('first_name', $errors);
required('last_name', $errors);
required('email', $errors);

// Validate fields for other rules
validateEmail('email', $errors);


echo '<pre>';
print_r($errors);

