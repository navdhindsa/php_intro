<?php

/**
 * Form 2
 * @file 16_list_view.php
 * @course Intro PHP, WDD 2018 Jan
 * @author Navdeep <dhindsanavdeep24@gmail.com>
 * @created_at 2018-8-31
 */


$title ='Detail view';
require '0_config.php';

if(empty($_GET['book_id'])){
  die('Please die!!!');
}

$book_id = intval($_GET['book_id']);



define('DB_USER','web_user');
define('DB_PASS','mypass');
define('DB_DSN','mysql:host=localhost;dbname=testing');
$dbh= new PDO(DB_DSN, DB_USER, DB_PASS);
//var_dump($dbh);

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$query = "SELECT 
          book.book_id,
          book.title,
          author.name as author,
          book.num_pages,
          year_published,
          genre.name as genre,
          format.name,
          publisher.name as publisher,
          book.price
          
          FROM book
          JOIN author using(author_id)
          JOIN publisher using(publisher_id)
          JOIN genre using(genre_id)
          JOIN format using(format_id)
          where book.book_id=:book_id";
$stmt = $dbh->prepare($query);
$stmt->bindValue(':book_id', $book_id,PDO::PARAM_INT);
$stmt ->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?=$title?></title>
  <style>
    table{
      border:1px solid #000;
      border-collapse: collapse;
    }
    td,th{
      border:1px solid #000;
      border-collapse: collapse;
    }
  </style>
  
</head>
<body>


<h1><?=$title?></h1>
  
  <?php foreach ($result as $row) : ?>
<table>
  <tr>
  <th>Field</th>
  <th>Value</th>
  </tr>
  <tr>
  <?php foreach ($row as $key => $value) : ?>
    
    <?php echo "<tr><td>$key</td><td>$value</td></tr>"  ?>
     <?php endforeach; ?>
  </tr>
  
  
  
  </table>
  <br /><br />
 <?php endforeach; ?>
</body>
</html>