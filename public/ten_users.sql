drop table if exists users;

create table users(
id int not null primary key auto_increment,
first_name varchar(255),
  last_name varchar(255),
  email varchar(255),
  password varchar(255),

created_at DATETIME not null default current_timestamp,
  updated_at DATETIME default null

);