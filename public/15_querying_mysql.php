<?php

/**
 * Form 2
 * @file 14_connecting_to_mysql.php
 * @course Intro PHP, WDD 2018 Jan
 * @author Navdeep <dhindsanavdeep24@gmail.com>
 * @created_at 2018-8-31
 */



$title ='Querying MySQL';
require '0_config.php';
require '../connect_db.inc.php';
//mysqli  MySQL Improved
//pdo - php data object
//dsn - data source name
$dbh= new PDO(DB_DSN,DB_USER,DB_PASS);

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$query = 'SELECT 
          book.title,
          book.price,
          author.name AS author,
          book.book_id AS id
          FROM book
          JOIN author USING(author_id)
          ';
$stmt = $dbh->prepare($query);
$stmt ->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

var_dump($dbh);
//var_dump($result);
//var_dump($result);


?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?=$title?></title>
  <style>
    table{
      border:1px solid #000;
      border-collapse: collapse;
    }
    td,th{
      border:1px solid #000;
      border-collapse: collapse;
    }
  </style>
</head>
<body>


<h1><?=$title?></h1>
<table>
  <tr>
  <th>Title</th>
  <th>Price</th>
  <th>Author</th>
  <th>ID</th>  
    
    
  </tr>
  <?php foreach($result as $row) : ?>
  <tr>
  <td><?=$row['title']?></td>
    <td><?=$row['price']?></td>
    <td><?=$row['author']?></td>
    <td><?=$row['id']?></td>
  
  
  </tr>
  <?php endforeach;?>
  
  
  </table>

</body>
</html>