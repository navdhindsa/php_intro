<?php


$title= "Publisher's list";
include 'config.php';
include 'functions.php';

$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


$publishers= getPub($dbh);

?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Intro PHP Final Morning</title>
  </head>
  <body>
  <h1><?=$title?></h1>
  <ul>
    
    <ul>
          <?php foreach($publishers as $row) : ?>
      <li><a href="edit_publisher.php?publisher_id=<?=$row['publisher_id']?>"><?=$row['name']?></a></li>
                      <?php endforeach; ?>   
                  </ul>
    
    
    
    </ul>
  
  
  </body>
</html>