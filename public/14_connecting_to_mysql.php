<?php

/**
 * Form 2
 * @file 14_connecting_to_mysql.php
 * @course Intro PHP, WDD 2018 Jan
 * @author Navdeep <dhindsanavdeep24@gmail.com>
 * @created_at 2018-8-31
 */



$title ='Connecting to MySQL';
require '0_config.php';
require '../connect_db.inc.php';
//mysqli  MySQL Improved
//pdo - php data object
//dsn - data source name
$dbh= new PDO(DB_DSN,DB_USER,DB_PASS);

$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


var_dump($dbh);

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?=$title?></title>
</head>
<body>


<h1><?=$title?></h1>



</body>
</html>