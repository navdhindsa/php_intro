<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
/**
* Description
*@file 2_variables.php
*@author Navdeep Dhindsa <dhindsanavdeep24@gmail.com>
*@created_at 2018-07-27
*/

$title="Form 1 in PHP";



?><!DOCTYPE>
<html>
<head>
  <meta charset="utf-8" />
  <title><?=$title?></title>
</head>
<body>

	<h1><?php echo $title;?></h1>
	<form action="7_handle_form.php" method="post" accept-charset="UTF-8" novalidate="novalidate">

		<fieldset>

			<legend>Contact Us</legend>
		    <p><label for="name">Name</label>
			<input type="text" name="name" maxlength="255"></p>
			<p><label for="email">Email</label>
			<input type="text" name="email" maxlength="255"></p>
			
			<p><label for="comment">Comment</label>
			<textarea name="comment" ></textarea> </p>
           
            <p><label for="interest">Interest</label><br>
			<p><input type="checkbox" name="interest[]" value="c++">C++</p>
			<p><input type="checkbox" name="interest[]" value="C">C</p>
			<p><input type="checkbox" name="interest[]" value="JAVA">JAVA</p>
			<p><input type="checkbox" name="interest[]" value="Python">Python</p>
			<p><input type="checkbox" name="interest[]" value="php">php</p>
            <label for="gender">Gender</label>
            <select name="gender">&nbsp;
            	<option value="">Please select</option>
            	<option value="M">Male</option>
                <option value="F">Female</option>
                <br></select>
                <p><input type="radio" name="sub" value="yes">Subscribe</p>
                <p><input type="radio" name="sub" value="no">Don't Subscribe</p>
		<p><button type="submit">Submit</button></p>
		</fieldset>
			

</html>
