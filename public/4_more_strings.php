<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
/**
* Description
*@file 2_variables.php
*@author Navdeep Dhindsa <dhindsanavdeep24@gmail.com>
*@created_at 2018-07-27
*/

$title="More Strings in PHP";


$len_title=strlen($title);
$lower = strtolower($title);

$upper = strtoupper($title);
$field= 'first_name';
$abc=ucwords(str_replace('_', ' ', $field));
?><!DOCTYPE>
<html>
<head>
  <meta charset="utf-8" />
  <title><?=$title?></title>
</head>
<body>

	<h1><?php echo $title;?></h1>
<?php echo $len_title;?><br>
 <?=$lower?><br>
 <?=$upper?><br>
 <?=$abc?>
</html>
