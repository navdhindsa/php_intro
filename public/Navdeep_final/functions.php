<?php

/* YOUR FUNCTIONS GO HERE */





function first($dbh){
  $query = 'select count(title) from book where in_print=1';
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $one= $stmt->fetchAll(PDO::FETCH_ASSOC);
 return $one;
}

function second($dbh){
  $query = 'select count(title) from book where in_print=0';
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $two= $stmt->fetchAll(PDO::FETCH_ASSOC);
 return $two;
}


function third($dbh){
  $query = 'select format(avg(price),2) from book';
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $three= $stmt->fetchAll(PDO::FETCH_ASSOC);
 return $three;
}


function fourth($dbh){
  $query = 'select min(price) from book';
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $four= $stmt->fetchAll(PDO::FETCH_ASSOC);
 return $four;
}


function fifth($dbh){
  $query = 'select max(price) from book ';
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $five= $stmt->fetchAll(PDO::FETCH_ASSOC);
 return $five;
}


function sixth($dbh){
  $query = 'select count(name) from publisher';
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $six= $stmt->fetchAll(PDO::FETCH_ASSOC);
 return $six;
}


function seventh($dbh){
  $query = 'select count(publisher.name) from book right join publisher using(publisher_id) where book.title is null ';
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  $six= $stmt->fetchAll(PDO::FETCH_ASSOC);
 return $six;
}
