<?php

/**
 * Handle Form
 * @file 7_handle_form.php
 * @course Intro PHP, WDD 2018 Jan
 * @author Steve George <edu.pagerange@gmail.com>
 * @created_at 2018-07-27
 */

include('0_config.php');

if($_SERVER['REQUEST_METHOD'] !== 'POST') {
    die('Please submit the form!');
}

$title ='Handle Form';

// set empty errors array
$errors = [];

// empty return true if a) a variable is not set, or b)
// a variable is set but contains a falsey value"
// '', null, 0, false
if(empty($_POST['name'])) {
    $errors[] = 'Please enter a value in the name field';
} else {
    $name = htmlspecialchars($_POST['name']);    
}

if(empty($_POST['email'])) {
    $errors[] = 'Please enter a value in the email field';
} else {
    $email = htmlspecialchars($_POST['email']);    
}

if(empty($_POST['gender'])) {
    $errors[] = 'Please select a gender';
} else {
    $gender = htmlspecialchars($_POST['gender']);    

    if($gender == 'M') {
        $prefix = 'Mr.';
    } elseif($gender == 'F') {
        $prefix = 'Ms.';
    } else {
        $prefix = '';
    }
}

if(empty($_POST['comment'])) {
    $errors[] = 'Please enter a value in the comment field';
} else {
    $comment = htmlspecialchars($_POST['comment']);    
}

if(empty($_POST['subscribe'])) {
    $errors[] = 'Please make a subscription choice';
} else {
    $subscribe = htmlspecialchars($_POST['subscribe']);    
    if($subscribe == 'yes') {
        $thanks = 'We are so glad you decided to subscribe to our newsletter!';
    } else {
        $thanks = 'Please reconsider your decision not to subscribe.  We know where you live.';
    }

}


?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title><?=$title?></title>
</head>
<body>
<div id="output">

<h1><?=$title?></h1>

<h2>Output</h2>


<?php if(!empty($errors)) : ?>
    <div class="errors">
        <ul>
            <?php foreach($errors as $value) : ?>
                <li><?=$value?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php else : ?>
    <!-- output thankyou -->
    <?php
    
echo <<<EOT

<p>Thanks for contacting us, {$prefix} {$name}.  You left us the following wonderful comment:</p>

<p>{$comment}</p>

If your comment requires a response we will contact you at {$email}.</p>

<p>{$thanks}</p>

<p>Have a great day! <br />Management.</p>

EOT;
?>

<?php endif; ?>


<pre>

<?php print_r($_POST); ?>

</pre>

</div><!-- /#output -->

<div id="source">

    <h2>Source</h2>

    <?php show_source(__FILE__); ?>

</div><!-- /#source -->

</body>
</html>