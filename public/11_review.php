<?php

/**
* Description
*@file 11_review.php
*@author Navdeep Dhindsa <dhindsanavdeep24@gmail.com>
*@created_at 2018-8-01
*/



//tags
/* <?php     ?> */




//Request methods

//get
//http://example.com/page/about

//post
//form submissions

//where to find submitted info

//get requesr
//http://example.com?search=king
//look in $_GET

//get variables are appened to a URL in a query
//string, as key/value pairs - starting with a question marks
//each pair separated by ampersand

// ?first=bob&last=jones&city=winnipeg

//variables submitted by form

//look in $_POST

//how to print
//print
//echo



//variables

//constants

//basic debugging
//print_r  show only value and structure

//var_dump shows value, structure, data type and length

//die kills the script at the point of execution die('msg')


//template tags/alternative syntax

//php to use inside html
//if,elseif,else,loops
//use colons instead of curly braces
//end with written terminator
//endif

//endwhile
//endforeach


/*<?php if(!empty($errors)) : ?>

//show errors

<?php else : ?>

//do whatever

//<?php endif;  ?>

//form
//action- page 
//method-get or post
//accept-charset-utf-8
//novalidate- to stop html validation
//when to use get request
//search 
//bookmarkable
//only requesting info not changing

//post
//private info
//creating or updating data with forms


//finding submitted info

//method post- $_POST SuperGlobal
//method get- $_GET SuperGlobal

//never trust submitted data- number 1 rule


//superglobals
//built in arrays with info about submitted datda, server, environment,cookies, session

//$_POST - post data
//$_GET - get data
//$_REQUEST - both get and post data
//$_COOKIE - cookie info
//$_FILES- info about uploaded files
//$_SERVER - info about server, user and request
//$_ENV - the system environment
//$_GLOBALS



//form validation

//specifically, finding out if a user entered data in a prticular field
//empty- falsey value
//0,false,null, empty array, empty string
//isset- returns true only if a variable is set even if it is empty

//other validations

//string length strlen
//min value - if($_POST['age']<4)
