<?php

/**
 * Our functions file
 * @10_functions.php
 * @course Intro PHP, WDD 2018 Jan
 * @author Steve George <edu.pagerange@gmail.com>
 * @created_at 2018-07-31
 */

 // Some useful functions we can use

/**
 * Validate for required
 * @param String $field_name
 * @param Array &$errors (passed by reference)
 * @return Bool true if valid, false otherwise
 */
function required($field_name, &$errors)
{
    $label = ucfirst(str_replace('_', ' ', $field_name));
    if(empty($_POST[$field_name])) {
        $errors[$field_name] = "{$label} is a required field.";
        return false;
    }
    return true;
}

/**
 * Validate Email
 * @param String $field_name
 * @param Array &$errors (passed by reference)
 * @return Bool true if valid, false otherwise
 */
function validateEmail($field_name, &$errors)
{

    if(!filter_input(INPUT_POST, $field_name, FILTER_VALIDATE_EMAIL)) {
        $errors[$field_name] = "Please enter a valid email address";
        return false;
    }
    return true;
}