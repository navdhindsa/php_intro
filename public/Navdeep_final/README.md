# Intro PHP – Practical Final Project

You will have approximately 60 minutes to complete this practical component (until 4:00 PM).  

This project requires that the intro_practical database be up and running in MySQL, and that you can connect to MySQL through PHP.  If you are using your laptop, the database should already be set up.  If your laptop is not here, or not working, you may use one of the classroom computers, but you will need to use the `intro_practical.sql` file (included) to set up the database.  It is your responsibility to get it set up and running properly.

**Goal: write the PHP and MySQL queries to dynamically generate the output that currently exists in a sample file.**

## Follow these steps exactly

* Download the practical files from here: [http://dev.pagerange.com/wdd2018/final/practical.zip](http://dev.pagerange.com/wdd2018/final/practical.zip)
* Unzip the folder into the WAMP `www` folder
* Rename the folder to `yourname_final` - replace `yourname` with your firstname.
* Open `index.php` in an editor.  It is set to run.  You only need to add your code.
* You can create code in `functions.php` and `index.php`
* Write your queries to re-create the hard-coded output in `index.php`

## When you are finished

* Zip up the folder.  
* DO NOT INCLUDE ANY OTHER FILES.
* Email to: **edu.pagerange@gmail.com** BEFORE YOU LEAVE
* Confirm with the instructor that he has received your files before leaving the class, or you will receive ZERO marks for the practical component.
* Files received after 4:00 PM will receive ZERO marks.

### Good Luck!
