<?php

/**
 * Our functions file
 * @10_functions.php
 * @course Intro PHP, WDD 2018 Jan
 * @author Steve George <edu.pagerange@gmail.com>
 * @created_at 2018-07-31
 */

 // Some useful functions we can use

/**
 * Validate for required
 * @param String $field_name
 * @param Array &$errors (passed by reference)
 * @return Bool true if valid, false otherwise
 */
function required($field_name, &$errors)
{
    
    if(empty($_POST[$field_name])) {
        $errors[$field_name][] = "{$field_name} is a required field.";
       
    }
    return $errors;
}

function minLength($field_name, $min_length, $errors)
{
  if(strlen($_POST[$field_name])< $min_length){
    $errors[$field_name][]= "{$field_name} must be of at least $min_length characters";
  }
  return $errors;
}

/**
 * Validate Email
 * @param String $field_name
 * @param Array &$errors (passed by reference)
 * @return Bool true if valid, false otherwise
 */
function validateEmail($field_name, &$errors)
{

    if(!filter_input(INPUT_POST, $field_name, FILTER_VALIDATE_EMAIL)) {
        $errors[$field_name] = "Please enter a valid email address";
        return false;
    }
    return true;
}


function passwordsMatch($field1, $field2, $errors)
{
    if($_POST[$field1] !== $_POST[$field2]) {
        $errors['password'] = 'Passwords do not match';
    }
    return $errors;
}
/**
* Escape a string for use ingeneral HTML
* @param String $string the string to be escaped
* @return String the escaped string
*/
function esc($string) 
{
    return htmlspecialchars($string, NULL, 'UTF-8', false);
}

/**
* Escape a string for use in an attribute
* @param String $string the string to be escaped
* @return String the escaped string
*/
function esc_attr($string)
{
    return htmlspecialchars($string, ENT_QUOTES, "UTF-8", false);
}

